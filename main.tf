module "dms_replication_instance" {
  source                       = "./modules/dms_replication_instance"
  create                       = var.create_replication_instance
  vpc_id                       = var.replication_instance_vpc_id
  subnet_ids                   = var.replication_instance_subnet_ids
  allocated_storage            = var.replication_instance_allocated_storage
  apply_immediately            = var.replication_instance_apply_immediately
  auto_minor_version_upgrade   = var.replication_instance_auto_minor_version_upgrade
  availability_zone            = var.replication_instance_availability_zone
  engine_version               = var.replication_instance_engine_version
  kms_key_arn                  = var.replication_instance_kms_key_arn
  multi_az                     = var.replication_instance_multi_az
  preferred_maintenance_window = var.replication_instance_preferred_maintenance_window
  publicly_accessible          = var.replication_instance_publicly_accessible
  instance_class               = var.replication_instance_class
  instance_id                  = var.name
  tags                         = var.tags
}

resource "aws_dms_endpoint" "source" {
  endpoint_id                 = var.source_endpoint_id
  endpoint_type               = "source"
  engine_name                 = var.source_engine
  extra_connection_attributes = var.source_connection_attributes
  server_name                 = var.source_server_name
  database_name               = var.source_database_name
  username                    = var.source_username
  password                    = var.source_password
  port                        = var.source_port
  ssl_mode                    = var.ssl_mode
  tags                        = var.tags
}

resource "aws_dms_endpoint" "target" {
  endpoint_id                 = var.target_endpoint_id
  endpoint_type               = "target"
  engine_name                 = var.target_engine
  extra_connection_attributes = var.target_connection_attributes
  server_name                 = var.target_server_name
  database_name               = var.target_database_name
  username                    = var.target_username
  password                    = var.target_password
  port                        = var.target_port
  ssl_mode                    = var.ssl_mode
  tags                        = var.tags
}

resource "aws_dms_replication_task" "this" {
  migration_type            = var.migration_type
  replication_instance_arn  = local.replication_instance_arn
  replication_task_id       = var.name
  source_endpoint_arn       = aws_dms_endpoint.source.endpoint_arn
  target_endpoint_arn       = aws_dms_endpoint.target.endpoint_arn
  table_mappings            = var.table_mappings
  replication_task_settings = local.replication_task_settings
  tags                      = var.tags
}

resource "aws_dms_event_subscription" "this" {
  enabled          = var.create_event_subscription
  event_categories = var.event_subscription_event_categories
  name             = var.name
  sns_topic_arn    = var.event_subscription_sns_topic_arn
  source_ids       = [aws_dms_replication_task.this.replication_task_id]
  source_type      = "replication-task"
  tags             = var.tags
}
