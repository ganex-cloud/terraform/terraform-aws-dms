variable "name" {
  description = "Name of resources"
}

variable "migration_type" {
  description = "(Required) The migration type. Can be one of full-load | cdc | full-load-and-cdc."
  type        = string
  default     = "full-load"
}

variable "source_server_name" {
  description = "(Optional) The host name of the server."
  type        = string
  default     = null
}

variable "source_endpoint_id" {
  description = "(Required) The database endpoint identifier."
  type        = string
}

variable "source_engine" {
  description = "(Required) The type of engine for the endpoint. Can be one of aurora | aurora-postgresql| azuredb | db2 | docdb | dynamodb | elasticsearch | kafka | kinesis | mariadb | mongodb | mysql | oracle | postgres | redshift | s3 | sqlserver | sybase."
  type        = string
}

variable "source_database_name" {
  description = "(Optional) The name of the endpoint database."
  type        = string
  default     = null
}

variable "source_username" {
  description = "(Optional) The user name to be used to login to the endpoint database."
  type        = string
  default     = null
}

variable "source_password" {
  description = "(Optional) The password to be used to login to the endpoint database."
  type        = string
  default     = null
}

variable "source_port" {
  description = "(Optional) The port used by the endpoint database."
  type        = string
  default     = null
}

variable "source_connection_attributes" {
  description = "(Optional) Additional attributes associated with the connection. For available attributes see Using Extra Connection Attributes with AWS Database Migration Service (http://docs.aws.amazon.com/dms/latest/userguide/CHAP_Introduction.ConnectionAttributes.html)."
  type        = string
  default     = null
}

variable "target_server_name" {
  description = "(Optional) The host name of the server."
  type        = string
  default     = null
}

variable "target_endpoint_id" {
  description = "(Required) The database endpoint identifier."
  type        = string
}

variable "target_engine" {
  description = "(Required) The type of engine for the endpoint. Can be one of aurora | aurora-postgresql| azuredb | db2 | docdb | dynamodb | elasticsearch | kafka | kinesis | mariadb | mongodb | mysql | oracle | postgres | redshift | s3 | sqlserver | sybase."
  type        = string
}

variable "target_database_name" {
  description = "(Optional) The name of the endpoint database."
  type        = string
  default     = null
}

variable "target_username" {
  description = "(Optional) The user name to be used to login to the endpoint database."
  type        = string
  default     = null
}

variable "target_password" {
  description = "(Optional) The password to be used to login to the endpoint database."
  type        = string
  default     = null
}

variable "target_port" {
  description = "(Optional) The port used by the endpoint database."
  type        = string
  default     = null
}

variable "target_connection_attributes" {
  description = "(Optional) Additional attributes associated with the connection. For available attributes see Using Extra Connection Attributes with AWS Database Migration Service (http://docs.aws.amazon.com/dms/latest/userguide/CHAP_Introduction.ConnectionAttributes.html)."
  type        = string
  default     = null
}

variable "ssl_mode" {
  description = "(Optional) The SSL mode to use for the connection. Can be one of none | require | verify-ca | verify-full"
  type        = string
  default     = "none"
}

variable "replication_task_settings" {
  description = "(Optional) An escaped JSON string that contains the task settings. For a complete list of task settings, see Task Settings for AWS Database Migration Service Tasks. (http://docs.aws.amazon.com/dms/latest/userguide/CHAP_Tasks.CustomizingTasks.TaskSettings.html)"
  type        = string
  default     = null
}

variable "table_mappings" {
  description = "(Required) An escaped JSON string that contains the table mappings. For information on table mapping see Using Table Mapping with an AWS Database Migration Service Task to Select and Filter Data. (http://docs.aws.amazon.com/dms/latest/userguide/CHAP_Tasks.CustomizingTasks.TableMapping.html)"
  type        = string
  default     = <<EOF
{
   "rules":[
      {
         "rule-type":"selection",
         "rule-id":"1",
         "rule-name":"1",
         "object-locator":{
            "schema-name":"%",
            "table-name":"%"
         },
         "rule-action":"include",
         "filters":[
            
         ]
      }
   ]
}
EOF
}

variable "create_replication_instance" {
  description = "(Required) Create a replication instance."
  type        = bool
  default     = true
}

variable "replication_instance_vpc_id" {
  description = "(Required) VPC Id to use on Security Group."
  type        = string
  default     = null
}

variable "replication_instance_subnet_ids" {
  description = "(Required) A list of the EC2 subnet IDs for the subnet group."
  type        = list(string)
  default     = null
}

variable "replication_instance_arn" {
  description = "(Optional) Specifies the replication instance arn. You cannot set the replication_instance_arn parameter if the create_replication_instance parameter is set to true."
  type        = string
  default     = null
}

variable "replication_instance_allocated_storage" {
  description = "(Optional) The amount of storage (in gigabytes) to be initially allocated for the replication instance."
  type        = number
  default     = 50
}

variable "replication_instance_apply_immediately" {
  description = "(Optional) Indicates whether the changes should be applied immediately or during the next maintenance window. Only used when updating an existing resource."
  type        = bool
  default     = false
}

variable "replication_instance_auto_minor_version_upgrade" {
  description = "(Optional) Indicates that minor engine upgrades will be applied automatically to the replication instance during the maintenance window."
  type        = bool
  default     = false
}

variable "replication_instance_availability_zone" {
  description = "(Optional) The EC2 Availability Zone that the replication instance will be created in."
  type        = string
  default     = null
}

variable "replication_instance_engine_version" {
  description = "(Optional) The engine version number of the replication instance."
  type        = string
  default     = "3.4.3"
}

variable "replication_instance_kms_key_arn" {
  description = "(Optional) The Amazon Resource Name (ARN) for the KMS key that will be used to encrypt the connection parameters."
  type        = string
  default     = null
}

variable "replication_instance_multi_az" {
  description = "(Optional) Specifies if the replication instance is a multi-az deployment. You cannot set the availability_zone parameter if the multi_az parameter is set to true."
  type        = bool
  default     = false
}

variable "replication_instance_preferred_maintenance_window" {
  description = "(Optional) The weekly time range during which system maintenance can occur, in Universal Coordinated Time (UTC)."
  type        = string
  default     = null
}

variable "replication_instance_publicly_accessible" {
  description = "(Optional) Specifies the accessibility options for the replication instance. A value of true represents an instance with a public IP address. A value of false represents an instance with a private IP address."
  type        = bool
  default     = false
}

variable "replication_instance_class" {
  description = "(Required) The compute and memory capacity of the replication instance as specified by the replication instance class. Can be one of dms.t2.micro | dms.t2.small | dms.t2.medium | dms.t2.large | dms.c4.large | dms.c4.xlarge | dms.c4.2xlarge | dms.c4.4xlarge"
  type        = string
  default     = "t3.small"
}

variable "tags" {
  description = "(Optional) A map of tags to assign to the resource."
  type        = map(string)
  default     = {}
}


variable "create_event_subscription" {
  description = "(Required) Create a event subscription resource."
  type        = bool
  default     = false
}

variable "event_subscription_event_categories" {
  description = "(Optional) List of event categories to listen, replication task supporter are 'configuration change', 'state change', 'deletion, 'creation, 'failure'"
  type        = list(string)
  default     = ["configuration change", "state change", "deletion", "creation", "failure"]
}

variable "event_subscription_sns_topic_arn" {
  description = "(Required if create_event_subscription is enabled) SNS topic arn to send events on."
  type        = string
  default     = null
}
