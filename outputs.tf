output "replication_instance_security_group_id" {
  value       = var.create_replication_instance ? module.dms_replication_instance.replication_instance_security_group_id : null
  description = "The Security Group ID of replication instance"
}

output "replication_instance_arn" {
  value       = var.create_replication_instance ? module.dms_replication_instance.replication_instance_arn : null
  description = "The Amazon Resource Name (ARN) of the replication instance."
}
