rds.logical_replication = 1
max_replication_slots = 5
max_wal_senders = 10
wal_sender_timeout = 0

https://docs.aws.amazon.com/dms/latest/userguide/CHAP_Source.PostgreSQL.html