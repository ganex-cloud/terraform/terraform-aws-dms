module "iam_role_dms-vpc-role" {
  source                  = "terraform-aws-modules/iam/aws//modules/iam-assumable-role"
  version                 = "~> 3.0"
  create_role             = true
  trusted_role_services   = ["dms.amazonaws.com"]
  role_name               = "dms-vpc-role"
  role_requires_mfa       = false
  custom_role_policy_arns = ["arn:aws:iam::aws:policy/service-role/AmazonDMSVPCManagementRole"]
}

module "iam_role_dms-cloudwatch-logs-role" {
  source                  = "terraform-aws-modules/iam/aws//modules/iam-assumable-role"
  version                 = "~> 3.0"
  create_role             = true
  trusted_role_services   = ["dms.amazonaws.com"]
  role_name               = "dms-cloudwatch-logs-role"
  role_requires_mfa       = false
  custom_role_policy_arns = ["arn:aws:iam::aws:policy/service-role/AmazonDMSCloudWatchLogsRole"]
}
