data "local_file" "replication_task_settings" {
  filename = "${path.module}/files/replication-task-setttings-default.json"
}

locals {
  replication_instance_arn  = var.create_replication_instance ? module.dms_replication_instance.replication_instance_arn : var.replication_instance_arn
  replication_task_settings = var.replication_task_settings == null ? data.local_file.replication_task_settings.content : var.replication_task_settings
}
