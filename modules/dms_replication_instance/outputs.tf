output "replication_instance_security_group_id" {
  value       = aws_security_group.this[0].id
  description = "The Security Group ID of replication instance"
}

output "replication_instance_arn" {
  value       = aws_dms_replication_instance.this[0].replication_instance_arn
  description = "The Amazon Resource Name (ARN) of the replication instance."
}
