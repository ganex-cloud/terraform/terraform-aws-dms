resource "aws_dms_replication_subnet_group" "this" {
  count                                = var.create ? 1 : 0
  replication_subnet_group_description = "Replication subnet group for ${var.instance_id}"
  replication_subnet_group_id          = var.instance_id
  subnet_ids                           = var.subnet_ids
  tags                                 = var.tags
}

resource "aws_dms_replication_instance" "this" {
  count                        = var.create ? 1 : 0
  allocated_storage            = var.allocated_storage
  apply_immediately            = var.apply_immediately
  auto_minor_version_upgrade   = var.auto_minor_version_upgrade
  availability_zone            = var.availability_zone
  engine_version               = var.engine_version
  kms_key_arn                  = var.kms_key_arn
  multi_az                     = var.multi_az
  preferred_maintenance_window = var.preferred_maintenance_window
  publicly_accessible          = var.publicly_accessible
  replication_instance_class   = var.instance_class
  replication_instance_id      = var.instance_id
  replication_subnet_group_id  = aws_dms_replication_subnet_group.this[0].id
  tags                         = var.tags
  vpc_security_group_ids       = ["${aws_security_group.this[0].id}"]
}

resource "aws_security_group" "this" {
  count  = var.create ? 1 : 0
  name   = "${var.instance_id}-dms"
  vpc_id = var.vpc_id
  tags = merge(
    {
      "Name" = "${var.instance_id}-dms"
    },
    var.tags,
  )
}

resource "aws_security_group_rule" "egress" {
  count             = var.create ? 1 : 0
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.this[0].id
}
