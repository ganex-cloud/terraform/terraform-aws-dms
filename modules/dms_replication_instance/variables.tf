variable "create" {
  description = "(Required) Create resources."
  type        = bool
  default     = true
}

variable "instance_id" {
  description = "(Required) The replication instance identifier. This parameter is stored as a lowercase string."
  type        = string
}

variable "vpc_id" {
  description = "(Required) VPC Id to use on Security Group."
  type        = string
}

variable "subnet_ids" {
  description = "(Required) A list of the EC2 subnet IDs for the subnet group."
  type        = list(string)
}

variable "allocated_storage" {
  description = "(Optional) The amount of storage (in gigabytes) to be initially allocated for the replication instance."
  type        = number
  default     = 50
}

variable "apply_immediately" {
  description = "(Optional) Indicates whether the changes should be applied immediately or during the next maintenance window. Only used when updating an existing resource."
  type        = bool
  default     = false
}

variable "auto_minor_version_upgrade" {
  description = "(Optional) Indicates that minor engine upgrades will be applied automatically to the replication instance during the maintenance window."
  type        = bool
  default     = false
}

variable "availability_zone" {
  description = "(Optional) The EC2 Availability Zone that the replication instance will be created in."
  type        = string
  default     = null
}

variable "engine_version" {
  description = "(Optional) The engine version number of the replication instance."
  type        = string
  default     = "3.4.3"
}

variable "kms_key_arn" {
  description = "(Optional) The Amazon Resource Name (ARN) for the KMS key that will be used to encrypt the connection parameters."
  type        = string
  default     = null
}

variable "multi_az" {
  description = "(Optional) Specifies if the replication instance is a multi-az deployment. You cannot set the availability_zone parameter if the multi_az parameter is set to true."
  type        = bool
  default     = false
}

variable "preferred_maintenance_window" {
  description = "(Optional) The weekly time range during which system maintenance can occur, in Universal Coordinated Time (UTC)."
  type        = string
  default     = null
}

variable "publicly_accessible" {
  description = "(Optional) Specifies the accessibility options for the replication instance. A value of true represents an instance with a public IP address. A value of false represents an instance with a private IP address."
  type        = bool
  default     = false
}

variable "instance_class" {
  description = "(Required) The compute and memory capacity of the replication instance as specified by the replication instance class. Can be one of dms.t2.micro | dms.t2.small | dms.t2.medium | dms.t2.large | dms.c4.large | dms.c4.xlarge | dms.c4.2xlarge | dms.c4.4xlarge"
  type        = string
  default     = "t3.small"
}

variable "tags" {
  description = "(Optional) A map of tags to assign to the resource."
  type        = map(string)
  default     = {}
}
